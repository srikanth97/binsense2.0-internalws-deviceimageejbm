#!/usr/bin/env sh

usage() { echo -e "\nUSAGE:\n$0 [-c : COMMAND to execute in '' string.] [-s : SUBMODULES to execute on. Leave empty to do all ] [-r : set to RECURSE into each branch for each command]" 1>&2; exit 1; }

doAll=true;
while getopts ":c:s:r" o; do
    case "${o}" in
        c)
            subCmd=${OPTARG};   
            ;;
        s)
            s=${OPTARG};
            doAll=false
            ;;
        r)
            recurse=true
            ;;
        *)
            usage
            ;;
    esac
done

if [ -z "${subCmd}" ]; then
    usage
fi

parentDir=${PWD##*/}
parentBranch="$(git branch | grep \* | cut -d ' ' -f2)";

targetIndices=();
commaChunks=();
IFS=',' read -ra commaChunks <<< "$s" echo ${commaChunks[1]};
for iComma in "${commaChunks[@]}"; do
    IFS='-' read -ra commaValues <<< "$iComma" echo ${commaValues[1]};
    if [ "${#commaValues[@]}" -gt "1" ]; then
        for (( i = "${commaValues[0]}" ; i <= "${commaValues[1]}" ; i++ )); do
            targetIndices+=( $i );
        done
    else
        targetIndices+=( $iComma )
    fi
done

i=1;
#loop through all submodules, tracking index in i
while read childDir; do
    childDir="$(echo  "${childDir}" | sed -e 's/[[:space:]]*$//')"
    if [ "$recurse" = true ]
    then        
        eval "cd $childDir";
        childBranch="$(git branch | grep \* | cut -d ' ' -f2)";
    fi
#    do local work (2nd arg optional. Will limit
    if [ "$doAll" = true ] && [ "$childDir" != "submod" ]; then
        eval $subCmd;
    else 
#        loop through target indices
        for j in "${targetIndices[@]}"; do
            if [ "$i" == "$j" ]
            then
                eval $subCmd;
            fi
        done
    fi
#    increment var    
    i=$((i+1));
    if [ "$recurse" = true ]
    then        
        cd ../;
    fi
done <./listOfSubmodules.txt

